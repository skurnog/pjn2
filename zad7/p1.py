from zad7.parser import DocumentParser
from gensim.models.lsimodel import LsiModel
from gensim import corpora

if __name__ == '__main__':
    corpus = DocumentParser().parsed_corpus
    corpus_list = [doc_content for doc_id, doc_content in corpus.items()]
    corpus_dictionary = corpora.Dictionary(corpus_list)
    corpus_vectorized = [corpus_dictionary.doc2bow(document) for document in corpus_list]
    lsi = LsiModel(corpus_vectorized, num_topics=20) # nie działa mi mapping id-word :(
    print(lsi[corpus_dictionary.doc2bow(corpus_list[2])])
