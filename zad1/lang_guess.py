import math

from zad1.ngrams import *
from os import listdir


def metric(ngram_vec1, ngram_vec2, type_='euclid'):

    ngram_vec1 = normalize(ngram_vec1)
    ngram_vec2 = normalize(ngram_vec2)

    res = {
        'euclid': euclid,
        'taxi': taxi,
        'cosine': cosine,
        'max': max_
    }.get(type_)(ngram_vec1, ngram_vec2)

    return res


def euclid(ngram_vec1, ngram_vec2):
    return math.sqrt(sum([
        (ngram_vec1.get(i, 0) - ngram_vec2.get(i, 0)) ** 2
        for i in set(list(ngram_vec1.keys()) + list(ngram_vec2.keys()))
    ]))


def taxi(ngram_vec1, ngram_vec2):
    return sum([math.fabs(ngram_vec1.get(i, 0) - ngram_vec2.get(i, 0))
                for i in set(list(ngram_vec1.keys()) + list(ngram_vec2.keys())
    )])


def max_(ngram_vec1, ngram_vec2):
    return max([
                   math.fabs(ngram_vec1.get(i, 0) - ngram_vec2.get(i, 0))
                   for i in set(list(ngram_vec1.keys()) + list(ngram_vec2.keys()))
                   ])


def cosine(ngram_vec1, ngram_vec2):
    return 1 - sum([ngram_vec1.get(i, 0) * ngram_vec2.get(i, 0)
                for i in set(ngram_vec1.keys()).intersection(set(ngram_vec2.keys()))])\
               / (calc_len(ngram_vec1) * calc_len(ngram_vec2))


def calc_len(ngram_vec):
    return math.sqrt(sum(map(lambda x: x**2, ngram_vec.values())))


def normalize(ngram_vec):
    vec_len = calc_len(ngram_vec)
    return {key: value/vec_len for key, value in ngram_vec.items()}


# def calculate_ngram_vector_for_language(language_name, n=2):
#     ngram_vec = {}
#     for file in [lang for lang in listdir("resources") if lang[:-5] == language_name]:
#         ngram_vec = get_ngram_stat("resources/" + file, from_file=True, n=n, ngram_vec=ngram_vec)
#     return ngram_vec


def calculate_ngram_stats(n=2, small=False):
    ngram_stats = {}

    file_list = [file for file in listdir("resources") if file[-5] == "1"] if small else listdir("resources")
    for file in file_list:
        # print(file)
        ngram_stats[file] = get_ngram_stat("resources/" + file, from_file=True, n=n)
    return ngram_stats


def guess_lang_debug(sample, ngram_stats=None):
    ngram_stats = calculate_ngram_stats() if ngram_stats is None else ngram_stats
    sample_ngram_stat = get_ngram_stat(sample)

    closest = ('', 99999)
    for language_name, languange_stat in ngram_stats.items():
        metric_ = metric(languange_stat, sample_ngram_stat, type_='cosine')
        if metric_ < closest[1]:
            closest = (language_name, metric_)
    print("==============")
    print(sample)
    print(closest[0][:-5] + " " + str(closest[1]))


def guess_lang(sample, ngram_stats=None, n=2):
    ngram_stats = calculate_ngram_stats() if ngram_stats is None else ngram_stats
    sample_ngram_stat = get_ngram_stat(sample, n=n)

    closest = ('', 99999)
    for language_name, languange_stat in ngram_stats.items():
        metric_ = metric(languange_stat, sample_ngram_stat, type_='cosine')
        if metric_ < closest[1]:
            closest = (language_name, metric_)
    return closest[0][:-5]


if __name__ == '__main__':
    ngram_stats = calculate_ngram_stats()

    guess_lang_debug("Me parece que equivoqué la especialidad, y ahora voy a tener problemas para conseguir trabajo", ngram_stats=ngram_stats)
    guess_lang_debug("In the case of dictionaries, it's implemented at the C level. The details are available in PEP 234. In particular, the section titled \"Dictionary Iterators\":", ngram_stats=ngram_stats)
    guess_lang_debug("Wenn es dem internationalen Finanzjudentum in und außerhalb Europas gelingen sollte, die Völker noch einmal in einen Weltkrieg zu stürzen, dann wird das Ergebnis nicht der Sieg des Judentums sein, sondern die Vernichtung der jüdischen Rasse in Europa! ", ngram_stats=ngram_stats)
    guess_lang_debug("Suomen tasavalta eli Suomi (ruots. Republiken Finland, Finland) on valtio Pohjois-Euroopassa Itämeren rannalla.", ngram_stats=ngram_stats)
    guess_lang_debug("La pizza è un prodotto gastronomico salato consistente in un impasto solitamente a base di farina e acqua che viene spianato e condito con vari ingredienti come ad esempio pomodoro e mozzarella, e cotto al forno", ngram_stats=ngram_stats)
    guess_lang_debug("Szpunt – drewniany mały kołek lub czop, służący do zatykania otworu w beczce, czyli szpuntowania.[1] Szpunty stosowano najczęściej w beczkach nalewnych z klepek dębowych. ", ngram_stats=ngram_stats)