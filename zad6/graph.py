# -*- coding: utf-8 -*-
# !/usr/bin/env python
import pickle

from zad6.parser import DocumentParser
from zad6.tfidf import SearchEngine


class DistanceGraph:
    def __init__(self, k, parser=None, graph_corpus=None):
        self.k = k
        self.parser = parser if parser is not None else DocumentParser()
        self.original_corpus = self.parser.parsed_corpus
        self.corpus_len = len(self.original_corpus.items())
        self.graph_corpus = self.construct_graph_corpus() if graph_corpus is None else graph_corpus
        self.cosine_metric = SearchEngine.cosine_metric

    def construct_graph_document(self, document):
        graph_document = {}
        window_size = self.k if len(document) >= self.k else len(document)
        for window_down_limit in range(len(document) - window_size + 1):
            for i in range(window_down_limit, window_down_limit + window_size):
                edge = (document[window_down_limit], document[i])
                graph_document[edge] = graph_document.get(edge, 0) + 1
        return graph_document

    def construct_graph_corpus(self):
        print("Building graphs for {0}...".format(self.k))

        corpus_graph = {}
        i = 1
        for key, value in self.original_corpus.items():
            print('{0}/{1}: {2}'.format(i, self.corpus_len, key))
            corpus_graph[key] = self.construct_graph_document(value)
            i += 1
        return corpus_graph

    def dump(self):
        pickle.dump(self.graph_corpus, open('resources/bin/graph_corpus_' + str(self.k) + '.pickle', 'wb'))
        print('I TOOK A DUMP')

    def query(self, text, count=5):
        query = self.parser.parse_document(text)
        query_graph = self.construct_graph_document(query)
        distances_dict = {}

        for document in self.graph_corpus.items():
            distances_dict[document[0]] = self.cosine_metric(query_graph, document[1])
        sorted_result = [(document_id, val) for document_id, val in sorted(distances_dict.items(), key=lambda item: item[1])][:count]
        # return [(document_id, val, self.corpus[document_id]) for document_id, val in sorted_result]
        return [(document_id, val) for document_id, val in sorted_result]

if __name__ == '__main__':
    dg2 = DistanceGraph(k=2)
    dg2.dump()
    del dg2

    dg3 = DistanceGraph(k=3)
    dg3.dump()
    del dg3

    dg3 = DistanceGraph(k=5)
    dg3.dump()
    del dg3

    dg3 = DistanceGraph(k=10)
    dg3.dump()
