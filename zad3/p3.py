from zad3.p1 import get_word_rank


def count_hapax_legomena(rank):
    return len([word for word in rank if word[0] == 1])


def number_of_words_above_freq(rank):
    words_sum = sum([element[0] for element in rank])

    cumulative_freq = 0
    result = 0
    for element in rank:
        if cumulative_freq < words_sum/2:
            result += 1
            cumulative_freq += element[0]
        else:
            break
    return result

if __name__ == '__main__':
    rank = get_word_rank()
    print("hapax legomena:", count_hapax_legomena(rank))
    print(">50%:", number_of_words_above_freq(rank))
