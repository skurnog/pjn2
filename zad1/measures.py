from os import listdir
from zad1.ngrams import get_file_content, get_ngram_stat
from zad1.lang_guess import guess_lang, calculate_ngram_stats
import random


def get_random_samples(language_name, number_of_samples=50, max_sample_len=100, min_sample_len=10):
    file_list = [lang for lang in listdir("resources") if lang[:-5] == language_name]
    files_num = len(file_list)
    samples_per_file = int(number_of_samples/files_num + 7)

    samples = []
    for file_name in file_list:
        file_content = get_file_content("resources/" + file_name)
        file_len = len(file_content) - 1
        for i in range(samples_per_file):
            down_range = random.randint(0, file_len - max_sample_len - min_sample_len)
            up_range = random.randint(down_range, down_range + max_sample_len) + min_sample_len
            samples.append(file_content[down_range:up_range])
    return samples


def get_random_negative_samples(language_name, lang_list, number_of_samples=50, max_sample_len=100, min_sample_len=10):
    samples = []
    for language in [lang for lang in lang_list if lang != language_name]:
        samples += get_random_samples(language, number_of_samples=number_of_samples / (float(len(lang_list) - 1)))
    return samples


def classify_samples(language_name, sample_set, ngram_stats, n=2):
    positives = []
    negatives = []

    for sample in sample_set:
        result = guess_lang(sample, ngram_stats, n)
        if result == language_name:
            positives.append(result)
        elif result != language_name:
            negatives.append(result)
    return positives, negatives


def calc_precision(true_positives, false_positives):
    return len(true_positives)/(float(len(true_positives) + len(false_positives)))


def calc_recall(true_positives, false_negatives):
    return len(true_positives)/(float(len(true_positives) + len(false_negatives)))


def calc_f1(precision, recall):
    return 2 * (precision*recall/(precision+recall))


def calc_accuracy(true_positives, false_positives, true_negatives, false_negatives):
    return (len(true_positives) + len(true_negatives))/(float(len(true_positives) +
        len(true_negatives) + len(false_positives) + len(false_negatives)))


def calculate_stats():
    results_file = open("results.txt", mode='a')
    for n in range(6, 20):
        results_file.write("Generating {}-gram\n".format(n))
        print("Generating {}-gram".format(n))
        lang_stats = calculate_ngram_stats(n, small=True)
        languages = [lang[:-5] for lang in lang_stats.keys()]
        for language in languages:
            results_file.write("Language: " + language + "\n")
            print("Language: " + language)
            samples_positive = get_random_samples(language, number_of_samples=10)
            samples_negative = get_random_negative_samples(language, languages, number_of_samples=50)
            true_positives, false_negatives = classify_samples(language, samples_positive, lang_stats, n)
            false_positives, true_negatives = classify_samples(language, samples_negative, lang_stats, n)
            results_file.write("True positives {}, False positives {}, True negatives {}, False negatives {}\n".format(
                len(true_positives), len(false_positives), len(true_negatives), len(false_negatives)))
            print("True positives {}, False positives {}, True negatives {}, False negatives {}".format(
                len(true_positives), len(false_positives), len(true_negatives), len(false_negatives)))

            precision = calc_precision(true_positives, false_positives)
            recall = calc_recall(true_positives, false_negatives)
            f1 = calc_f1(precision, recall)
            accuracy = calc_accuracy(true_positives, false_positives, true_negatives, false_negatives)
            results_file.write("Precision {}, recall {}, f1 {}, accuracy {}\n".format(precision, recall, f1, accuracy))
            print("Precision {}, recall {}, f1 {}, accuracy {}".format(precision, recall, f1, accuracy))


def process_result_file(max_n=5):
    result_file_lines = open("results_test.txt").readlines()
    plot_data = {}


    for n in range(max_n):
        offset = n*6*3
        for lang_nr in range(6):
            lang_name = result_file_lines[offset+1][len("Language: "):-1]

            data_line = result_file_lines[offset+2].split(',')
            tpos_num = data_line[0][len("True positives "):]


            plot_data.get(lang_name, {})['t_pos'] = tpos_num
    return plot_data

if __name__ == '__main__':
    # calculate_stats()
    print(process_result_file())
