import matplotlib.pyplot as plt
from zad3.p1 import get_word_rank


def draw_plots(number_of_words=None):
    draw_zipf_plots(number_of_words)


def draw_zipf_plots(number_of_words=30):
    print("Drawing Zipf-Mandelbrot plots 1/2")
    rank = get_word_rank()[:number_of_words] if number_of_words is not None else get_word_rank()
    word_list = [element[1] for element in rank]
    freq_list = [element[0] for element in rank]

    plt.plot(range(len(word_list)), freq_list)
    if number_of_words is not None and number_of_words < 45:
        plt.xticks(range(len(word_list)), word_list, rotation='vertical')
    plt.title("Prawo Zipfa-Mandelbrota 1/2")
    plt.savefig("plots/zipf1.png")

    print("Drawing Zipf-Mandelbrot plots 2/2")
    cumulative_percentage_list = calculate_cumulative_percentages(rank, len(rank))
    plt.close()
    plt.plot(range(len(word_list)), cumulative_percentage_list)
    if number_of_words is not None and number_of_words < 45:
        plt.xticks(range(len(word_list)), word_list, rotation='vertical')
    plt.title("Prawo Zipfa-Mandelbrota 2/2")
    plt.savefig("plots/zipf2.png")


def calculate_cumulative_percentages(rank, number_of_words=30):
    cumulative_percentages = []
    words_sum = sum([element[0] for element in rank])
    for frequency in [element[0] for element in (rank[:number_of_words] if number_of_words is not None else rank)]:
        cumulative_percentages.append(
            frequency/float(words_sum) + cumulative_percentages[-1] if len(cumulative_percentages) > 0 else 0)

    return cumulative_percentages

if __name__ == '__main__':
    draw_plots(150)
