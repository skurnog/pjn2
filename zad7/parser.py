import codecs
import re


class DocumentParser:

    def __init__(self, original_corpus=None):
        self.stoplist = self.create_stoplist()
        self.synonyms = self.create_synonyms_dictionary('resources/odm.txt')
        self.raw_corpus = self.generate_raw_corpus(corpus_file_name='resources/pap.txt')
        self.parsed_corpus = self.parse_corpus(corpus_file_name='resources/pap.txt') if original_corpus is None else original_corpus
        # self.freq_vec_corpus = self.generate_freq_vectors()

    def create_stoplist(self):
        print('Generating stoplist...')
        stoplist = []
        file = codecs.open('resources/stoplist.txt', encoding='utf-8').readlines()
        for line in file:
            stoplist.append(line.strip())
        return stoplist

    def create_synonyms_dictionary(self, synonyms_file):
        print('Generating synonyms dict...')
        synonyms = {}
        file = codecs.open(synonyms_file, encoding='utf-8').readlines()
        for line in file:
            words = line.split(', ')
            for word in words[1:]:
                synonyms[word] = words[0]
        return synonyms

    def get_synonym(self, word):
        if self.synonyms is None:
            return word
        else:
            synonym = self.synonyms.get(word)
            return synonym or word

    def parse_document(self, document):
        raw_words = re.findall("[a-zA-ZżółćęśąźńŻÓŁĆĘŚĄŹŃ]+", document.lower(), re.DOTALL)

        words = []
        for word in raw_words:
            if word not in self.stoplist:
                words.append(self.get_synonym(word))
        return words

    def parse_corpus(self, corpus_file_name):
        print('Parsing corpus...')
        corpus = codecs.open(corpus_file_name, encoding='utf-8').read()
        raw_notes = re.findall("(#[\d]+)([^#]*)", corpus, re.DOTALL)

        notes = {}
        for key, value in raw_notes:
            notes[key] = self.parse_document(value)
        return notes

    def generate_raw_corpus(self, corpus_file_name):
        print('Generating raw document...')
        corpus = codecs.open(corpus_file_name, encoding='utf-8').read()
        raw_notes = re.findall("(#[\d]+)([^#]*)", corpus, re.DOTALL)

        raw_corpus = {}
        for key, value in raw_notes:
            raw_corpus[key] = value
        return raw_corpus

    def get_original_document(self, document_id):
        return self.raw_corpus.get(document_id, 'NIE MA CZEGO PLĄDROWAĆ')

    # def generate_freq_vectors(self):
    #     print("Generating frequency vectors...")
    #     result_list = []
    #     for document in self.parsed_corpus.values():
    #         freq_vec = {}
    #         for word in document:
    #             freq_vec[word] = freq_vec.get(word, 0) + 1
    #         result_list.append(freq_vec)
    #     return result_list
