import zad6.tfidf as tfidf
import zad6.graph as graph
import pickle
import codecs

DocumentParser = tfidf.DocumentParser
TfidfSearch = tfidf.SearchEngine
GraphSearch = graph.DistanceGraph


class SearchTester:

    def __init__(self):
        self.results_file = codecs.open('resources/results.txt', 'w', encoding='utf-8')
        self.original_corpus = pickle.load(open('resources/bin/original_corpus.pickle', 'rb'))
        self.corpus_graph_k2 = pickle.load(open('resources/bin/graph_corpus_2.pickle', 'rb'))
        self.corpus_graph_k3 = pickle.load(open('resources/bin/graph_corpus_3.pickle', 'rb'))
        self.corpus_graph_k5 = pickle.load(open('resources/bin/graph_corpus_5.pickle', 'rb'))
        self.corpus_graph_k10 = pickle.load(open('resources/bin/graph_corpus_10.pickle', 'rb'))
        self.corpus_tfidf = pickle.load(open('resources/bin/tfidf_corpus.pickle', 'rb'))
        self.parser = DocumentParser(original_corpus=self.original_corpus)

    def calculate_precision(self, relevant_documents, retrieved_documents):
        return len(relevant_documents + retrieved_documents) / len(retrieved_documents)

    def calculate_recall(self, relevant_documents, retrieved_documents):
        return len(relevant_documents + retrieved_documents) / len(relevant_documents)

    def calculate_f1(self, precision_score, recall_score):
        return (precision_score * recall_score) / (precision_score + recall_score)

    def dump_result(self, result, algorithm_name):
        print("Dumping " + algorithm_name)
        self.results_file.write('{0}:\n'.format(algorithm_name))
        for document_id, score in result:
            document_content = self.parser.get_original_document(document_id)
            self.results_file.write("{0}\nSCORE: {1}\n{2}\n\n".format(document_id, score, document_content))
        self.results_file.write('\n')

    def test_graphs(self, sample_note):
        gs2 = GraphSearch(2, parser=self.parser, graph_corpus=self.corpus_graph_k2)
        self.dump_result(gs2.query(sample_note, 10), 'Graph k=2')

        gs3 = GraphSearch(2, parser=self.parser, graph_corpus=self.corpus_graph_k3)
        self.dump_result(gs3.query(sample_note, 10), 'Graph k=3')

        gs5 = GraphSearch(2, parser=self.parser, graph_corpus=self.corpus_graph_k5)
        self.dump_result(gs5.query(sample_note, 10), 'Graph k=5')

        gs10 = GraphSearch(2, parser=self.parser, graph_corpus=self.corpus_graph_k10)
        self.dump_result(gs10.query(sample_note, 10), 'Graph k=10')

    def test_tfidf(self, sample_note):
        tfidfs = TfidfSearch(parser=self.parser, tfidf_corpus=self.corpus_tfidf)
        self.dump_result(tfidfs.query(sample_note, 10), 'TF-IDF')

    def run_tests(self):
        # krul = r'Mianowanie dowódcy sił zbrojnych, zniesienie powszechnego poboru ' \
        #               r'oraz większą aktywność w dziedzinie zwalczania przestępczości ' \
        #               r'zapowiedział Janusz Korwin-Mikke - kandydat na prezydenta. ' \
        #               r'Hasłem kampanii wyborczej prezesa Unii Polityki Realnej Korwin-' \
        #               r'Mikkego ma być: "Dwam tysiące prominentów do kryminału na 2000 ' \
        #               r'rok". Korwin-Mikke chce, aby do więzienia trafili ci politycy i ' \
        #               r'urzędnicy państwowi, o których powszechnie wiadomo, że są ' \
        #               r'skorumpowani.'
        # self.test_graphs(krul)
        # self.test_tfidf(krul)

        pogoda = r'W sobotę w województwach wschodnich słonecznie. W pozostałej części' \
                 r'Polski zachmurzenie małe i umiarkowane, miejscami wzrastające do  dużego' \
                 r'z przelotnymi opadami deszczu i burzami. Na Wybrzeżu i Pomorzu' \
                 r'Zachodnim pochmurno. Temp. maks. od 16 st. na Nizinie Szczecińskiej do' \
                 r'27 st. w Małopolsce. Wiatr umiarkowany, z kierunków wschodnich. ' \
                 r'W niedzielę pogodnie, tylko w woj. zachodnich i południowych przelotny' \
                 r'deszcz i burza. Temp. min. od 8 do 13 st. Temp. maks. od 18 st. na' \
                 r'Wybrzeżu do 26 st. na południu. Wiatr słaby i umiarkowany, ze wschodu. '
        self.test_graphs(pogoda)
        self.test_tfidf(pogoda)


if __name__ == '__main__':
    tester = SearchTester()
    tester.run_tests()
