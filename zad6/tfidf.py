import math
import pickle
from zad6.parser import DocumentParser


class SearchEngine:

    def __init__(self, parser=None, tfidf_corpus=None):
        self.parser = parser if parser is not None else DocumentParser()
        self.original_corpus = self.parser.parsed_corpus
        self.corpus_len = len(self.original_corpus.keys())
        self.df_vector = {}
        self.tfidf_corpus = tfidf_corpus if tfidf_corpus is not None else self.calculate_tfidf_corpus()

    def calculate_tf(self, term, document):
        tf = 0
        for word in document:
            if word == term:
                tf += 1
        return tf

    def calculate_df(self, term):
        df = self.df_vector.get(term)
        if df is None:
            df = 1
            for document_id in self.original_corpus.keys():
                if term in self.original_corpus[document_id]:
                    df += 1
            self.df_vector[term] = df
        return df

    def calculate_tfidf(self, word, document):
        return self.calculate_tf(word, document) * math.log10(self.corpus_len / self.calculate_df(word))

    def calculate_tfidf_vector(self, document):
        tfidf_vector = {}
        for word in document:
            if word not in tfidf_vector.keys():
                tfidf_vector[word] = self.calculate_tfidf(word, document)
        return tfidf_vector

    def calculate_tfidf_corpus(self):
        print('Calculating tf-idf corpus vectors...')
        tfidf_documents = {}
        note_nr = 0
        for document in self.original_corpus.items():
            tfidf_documents[document[0]] = self.calculate_tfidf_vector(document[1])
            print('{0}/{1} {2}'.format(note_nr+1, self.corpus_len, tfidf_documents[document[0]]))
            note_nr += 1
        return tfidf_documents

    @staticmethod
    def _calculate_vector_length(ngram_vec):
        return math.sqrt(sum(map(lambda x: x ** 2, ngram_vec.values())))

    @staticmethod
    def cosine_metric(vec1, vec2):
        return 1 - sum([vec1.get(i, 0) * vec2.get(i, 0)
                        for i in set(vec1.keys()).intersection(set(vec2.keys()))]) \
                   / (SearchEngine._calculate_vector_length(vec1) * SearchEngine._calculate_vector_length(vec2))

    def dump(self):
        pickle.dump(self.tfidf_corpus, open('resources/bin/tfidf_corpus.pickle', 'wb'))
        pickle.dump(self.original_corpus, open('resources/bin/original_corpus.pickle', 'wb'))
        print('I TOOK A DUMP')

    def query(self, text, count=5):
        tfidf_vector = self.calculate_tfidf_vector(self.parser.parse_document(text))
        distances_dict = {}

        for document in self.tfidf_corpus.items():
            distances_dict[document[0]] = self.cosine_metric(tfidf_vector, document[1])
        sorted_result = [(document_id, val) for document_id, val in sorted(distances_dict.items(), key=lambda item: item[1])][:count]
        return [(document_id, val) for document_id, val in sorted_result]


if __name__ == '__main__':
    se = SearchEngine()
    se.dump()
