from zad1.ngrams import get_ngram_stat
from zad3.p1 import transform_to_basic_forms, get_forms_vec
from util import *


def calculate_stats():
    text = get_file_content("resources/potop.txt")
    forms_vec = get_forms_vec()

    basic_forms_text = transform_to_basic_forms(text, forms_vec)
    digram_stats = get_ngram_stat(basic_forms_text, from_file=False, method='word', n=2)
    trigram_stats = get_ngram_stat(basic_forms_text, from_file=False, method='word', n=3)
    return digram_stats, trigram_stats


if __name__ == '__main__':
    digram_stats, trigram_stats = calculate_stats()
    print(digram_stats)
    print(trigram_stats)
